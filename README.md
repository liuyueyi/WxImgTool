## 图文小工具

微信图文小工具前端源码


### 下边topbar

```
"tabBar": {
    "color": "#000",
    "selectedColor": "#2980b9",
    "backgroundColor": "#fff",
    "borderStyle": "white",
    "list": [
      {
        "pagePath": "pages/create/template/template",
        "text": "静态图文",
        "iconPath": "pages/images/ic_img_gray.png",
        "selectedIconPath": "pages/images/ic_img_blue.png"
      },
      {
        "pagePath": "pages/index/index",
        "text": "动态图文",
        "iconPath": "pages/images/ic_gif_gray.png",
        "selectedIconPath": "pages/images/ic_gif_gray.png"
      },
      {
        "pagePath": "pages/index/index",
        "text": "个人页",
        "iconPath": "pages/images/ic_about_gray.png",
        "selectedIconPath": "pages/images/ic_about_gray.png"
      }
    ]
  }
```