//app.js
App({
  onLaunch: function () {
    // 展示本地存储能力
    var logs = wx.getStorageSync('logs') || []
    logs.unshift(Date.now())
    wx.setStorageSync('logs', logs)

    // 登录
    wx.login({
      success: res => {
        // 发送 res.code 到后台换取 openId, sessionKey, unionId
      }
    })
    // 获取用户信息
    wx.getSetting({
      success: res => {
        if (res.authSetting['scope.userInfo']) {
          // 已经授权，可以直接调用 getUserInfo 获取头像昵称，不会弹框
          wx.getUserInfo({
            success: res => {
              // 可以将 res 发送给后台解码出 unionId
              this.globalData.userInfo = res.userInfo

              // 由于 getUserInfo 是网络请求，可能会在 Page.onLoad 之后才返回
              // 所以此处加入 callback 以防止这种情况
              if (this.userInfoReadyCallback) {
                this.userInfoReadyCallback(res)
              }
            }
          })
        }
      }
    })
  },
  globalData: {
    userInfo: null,
    host: "https://media.hhui.top/",
    currentType: '',
    imgMenuList: {
        currentMenu: 0,
        types: [
          {
            title: "图文创建",
            value: "/pages/create/infoImg/infoImg",
            is_show: true
          },
          {
            title: "模板图文",
            value: "/pages/create/template/template",
            is_show: true
          },
          {
            title: "添加水印",
            value: "/pages/create/water/water",
            is_show: true
          },
          {
            title: "二维码生成",
            value: "/pages/create/qrcode/gen/gen",
            is_show: true
          },
          {
            title: "二维码解析",
            value: "/pages/create/qrcode/decode/decode",
            is_show: true
          }
          ,
          {
            title: "markdown转图片",
            value: "/pages/create/markdown/markdown",
            is_show: true
          }
        ]
    }
  }
})