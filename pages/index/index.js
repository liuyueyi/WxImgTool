//index.js
//获取应用实例
const app = getApp()

Page({
  data: {
    menuList: {
      types: [
        {
          title: "静图生成Gif",
          value: "imgCreate",
          is_show: true
        },
        {
          title: "静图+动文生成Gif",
          value: "watermark",
          is_show: true
        },
        {
          title: "Gif+静态文字生成Gif",
          value: "imgCreate",
          is_show: true
        },
        {
          title: "Gif+动文生成Gif",
          value: "watermark",
          is_show: true
        }
      ]
    },
  },

  changeType: function(e) {
    var value = e.currentTarget.dataset.value;
    console.log(value);
  }

})
