// pages/create/qrcode/gen/gen.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    menuList: {},
    submitDisabled: false,
    containPadding: true
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    // 设置menu
    var menu = getApp().globalData.imgMenuList;
    this.setData({
      menuList: menu
    });
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  },

  changeType: function (e) {
    var value = e.currentTarget.dataset.value;
    var index = e.currentTarget.dataset.index;
    console.log(value);

    if (getApp().globalData.imgMenuList.currentMenu == index) {
      return;
    }

    getApp().globalData.imgMenuList.currentMenu = index;
    wx.redirectTo({
      url: value
    })
    // if (value == "/pages/create/template/template") {
    //   wx.switchTab({
    //     url: value
    //   })
    // } else {
    //   wx.navigateTo({
    //     url: value
    //   })
    // }
  },



  bindFormSubmit: function (e) {
    var content = e.detail.value.textarea;
    if (content == null || content.length == 0) {
      console.log("内容为空!");
      wx.showToast({
        title: '请输入markdown文本',
        icon: 'success',
        duration: 1500
      });
      return;
    }

    var path = getApp().globalData.host;
    var that = this;


    this.data.submitDisabled = true;

    var params = {
      content: content
    }
    console.log("params:", params);
    wx.request({
      url: path + 'wx/wx/md2img', //仅为示例，非真实的接口地址
      // url: "http://127.0.0.1:8080/wx/md2img",
      // header: {
      //   'Content-Type': 'application/json'
      // },
      data: params,
      success: function (res) {
        wx.hideToast();
        that.setData({ submitDisabled: false });

        var ans = res.data;

        // var ans = JSON.parse(data);
        console.log(ans);
        if (ans['status']['code'] == 200) {
          var imgUrl = path + ans.result.img;
          console.log("img", imgUrl);
          wx.previewImage({
            current: imgUrl, // 当前显示图片的http链接
            urls: [imgUrl] // 需要预览的图片http链接列表
          });
        } else {
          wx.showToast({
            title: '生成失败',
            icon: 'loading',
            duration: 1500
          });
        }
      },
      fail: function () {
        wx.showToast({
          title: '生成失败!',
          duration: 1500
        });
        that.setData({ submitDisabled: false });
      }
    })



  },

})