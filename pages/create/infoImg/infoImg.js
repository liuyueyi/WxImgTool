// pages/create/infoImg/infoImg.js
var imageUtil = require('../../../utils/util.js');

Page({
  data: {
    menuList: {},
    imgEnable:true,
    templateId: "vlc",
    templateImg: "https://media.hhui.top/ximg/template/vlc.png",
    signEnable: false,
    signText: "",
    disabled: false,
    contents: "",
    imagewidth: 0,//缩放后的宽  
    imageheight: 0,//缩放后的高  
    leftSize: 0,

    // 模板    
    templateImgList: [],
    templateImages: [],
    templateIndex : 0
  },
  changeType: function (e) {
    var value = e.currentTarget.dataset.value;
    var index = e.currentTarget.dataset.index;
    console.log(value);

    if (getApp().globalData.imgMenuList.currentMenu == index) {
      return;
    }

    getApp().globalData.imgMenuList.currentMenu = index;
    wx.redirectTo({
      url: value
    })

    // if (value == "/pages/create/template/template") {
    // wx.switchTab({
    // url: value
    // })
    // } else {
    // wx.navigateTo({
    // url: value
    // })
    // }
  },


  onLoad: function (option) {
    var path = getApp().globalData.host;
    var globalData = getApp().globalData;

   

    // 设置menu
    var menu = getApp().globalData.imgMenuList;
    this.setData({
      menuList: menu
    });

    console.log("create", option);
    var that = this;
    this.setData({
      templateId: option.id,
      templateImg: option.img
    });

    wx.request({
      url: 'https://media.hhui.top/wx/wx/list', //仅为示例，并非真实的接口地址
      header: {
        'content-type': 'application/json' // 默认值
      },
      success: function (res) {
        // var data = JSON.parse(res.data);
        var data = res.data;
        console.log(data);

        if (data.status.code == 200) {
          var originList = data.result.list;
          var itemList = [];
          var imgList = [];
          for (var i = originList.length - 1, j = 0; i >= 0; i-- , j++) {
            var tmp = {};
            tmp.name = originList[i].name;
            tmp.id = originList[i].id;
            tmp.img = path + originList[i].img;
            itemList[j] = tmp;
            imgList[j] = tmp.img;
          }

          that.setData({
            templateImgList: itemList,
            templateImages: imgList
          });

        } else {
          wx.showToast({
            title: data.result.msg,
            icon: 'error',
            duration: 1500
          })
        }
      }
    })
  },
  imageLoad: function (e) {
    if(this.data.imagewidth > 0) {
      return;
    }
    var imageSize = imageUtil.imageUtil(e)
    this.setData({
      imagewidth: imageSize.imageWidth,
      imageheight: imageSize.imageHeight,
      leftSize: imageSize.left
    })
  },
  swapper: function(e) {
    console.log(e);
    this.data.templateIndex = e.detail.current;
  },

  chooseMainImg: function (e) {
    var that = this;
    if (this.data.img != null && this.data.img.length > 0) {
      this.data.img = [];
    }
    wx.chooseImage({
      count: 1,
      sizeType: ['original', 'compressed'], // 可以指定是原图还是压缩图，默认二者都有
      sourceType: ['album', 'camera'], // 可以指定来源是相册还是相机，默认二者都有
      success: function (res) {
        // 返回选定照片的本地文件路径列表，tempFilePath可以作为img标签的src属性显示图片
        that.setData({
          img: res.tempFilePaths
        });
      }
    })
  },
  previewImageTemplates: function (e) {
    wx.previewImage({
      current: e.currentTarget.id, // 当前显示图片的http链接
      urls: this.data.templateImages,  // 需要预览的图片http链接列表
      success: function (e) {
        console.log(e);
      }
    })
  },
  previewImage: function (e) {
    wx.previewImage({
      current: e.currentTarget.id, // 当前显示图片的http链接
      urls: [e.currentTarget.id] // 需要预览的图片http链接列表
    })
  },
  // 是否收起图片
  imgEnableChange: function(e) {
    this.setData({
      imgEnable:  e.detail.value
    })
  },
  // 是否启用签名
  signChange: function (e) {
    this.setData({
      signEnable: e.detail.value
    });
    console.log(this.data);
  },

  imgCreate: function (e) {
    console.log("here", e.detail.value.textarea);
  },

  bindFormSubmit: function (e) {
    var tid = this.data.templateImgList[this.data.templateIndex].id;

    if (this.data.img == null || this.data.img.length == 0) {
      wx.showToast({
        title: '请选择一张图片',
        icon: 'success',
        duration: 1500
      });
      return;
    }

    if (this.data.disabled) {
      return;
    }

    // 设置btn为不可点击
    this.setData({ disabled: true });
    wx.showToast({
      title: '努力生成中...',
      icon: 'loading'
    });

    var path = getApp().globalData.host;
    var that = this;


    // 设置提交的参数
    var msg = e.detail.value.textarea;
    // var msg = "酬杜八同宿西山兰若\n周弼\n\n西山龙井路，俱有旧行踪。\n待得城中暇，重期石上逢。\n冷霜粘破屐，落月带残钟。\n拟践相邻约，烦君买一峰。";
    var id = tid;
    var tempFile = this.data.img[0];
    var signEnable = this.data.signEnable ? 1 : 0;
    var sign = e.detail.value.signarea;
    console.log("request", signEnable, sign);
    wx.uploadFile({
      url: path + 'wx/wx/create',
      // url: 'http://127.0.0.1:8080/wx/create', //仅为示例，非真实的接口地址
      header: {
        'Content-Type': 'application/json'
      },
      filePath: tempFile,
      name: 'image',
      formData: {
        'msg': msg,
        "templateId": id,
        "signStatus": signEnable,
        "sign": sign
      },
      success: function (res) {
        wx.hideToast();
        that.setData({ disabled: false });

        var data = res.data;

        var ans = JSON.parse(data);
        console.log(ans);
        if (ans['status']['code'] == 200) {
          // var imgUrl = ans['result']['prefix'] + ans['result']['base64result'];
          var imgUrl = path + ans.result.img;
          console.log("img", imgUrl);
          wx.previewImage({
            current: imgUrl, // 当前显示图片的http链接
            urls: [imgUrl] // 需要预览的图片http链接列表
          });



        } else {
          wx.showToast({
            title: '生成失败',
            icon: 'loading',
            duration: 1500
          });
        }
      },
      fail: function () {
        wx.showToast({
          title: '生成失败!',
          duration: 1500
        });
        that.setData({ disabled: false });
      }
    })

  }
});