//index.js
//获取应用实例
var imageUtil = require('../../../utils/util.js');
const app = getApp()

Page({
  data: {
    menuList: {},
    windowW: 0,
    windowH: 0,
    list: [],
    userInfo: {},
    hasUserInfo: false,
    canIUse: wx.canIUse('button.open-type.getUserInfo')
  },
  imageLoad: function (e) {
    var that = this;
    wx.getSystemInfo({
      success: function (res) {
        that.setData({
          windowW: res.windowWidth,
          windowH: res.windowHeight
        })
      }
    })
  },
  onLoad: function () {
    var path = getApp().globalData.host;
    var that = this;

    // 设置menu
    var menu = getApp().globalData.imgMenuList;
    this.setData({
      menuList: menu
    });

    wx.request({
      url: 'https://media.hhui.top/wx/wx/list', //仅为示例，并非真实的接口地址
      header: {
        'content-type': 'application/json' // 默认值
      },
      success: function (res) {
        // var data = JSON.parse(res.data);
        var data = res.data;

        if (data.status.code == 200) {
          var originList = data.result.list;
          var itemList = [];
          for (var i = originList.length - 1, j = 0; i >= 0; i-- , j++) {
            var tmp = {};
            tmp.name = originList[i].name;
            tmp.id = originList[i].id;
            tmp.img = path + originList[i].img;

            var wW = that.data.windowW - 10, wH = that.data.windowH - 10;
            if (wW < 0) {
              wx.getSystemInfo({
                success: function (res) {
                  wW = res.windowWidth - 20;
                  wH = res.windowHeight - 20
                }
              })
            }

            var oW = originList[i].imgW, oH = originList[i].imgH;
            // console.log(wW, wH, oW, oH);
            if (oW > wW) {
              tmp.w = wW;
              tmp.h = (tmp.w * oH) / oW;
              tmp.left = 0;
            } else {
              tmp.w = oW;
              tmp.h = oH;
              tmp.left = (wW - tmp.w) / 2;
            }

            // console.log(tmp);

            itemList[j] = tmp;
          }
          that.setData({
            list: itemList
          });

        } else {
          wx.showToast({
            title: data.result.msg,
            icon: 'error',
            duration: 1500
          })
        }
      }
    })
  },

  temclick: function (option) {
    var content = option['currentTarget']['dataset']['content'];

    wx.navigateTo({
      url: '../create?id=' + content['id']
      + "&img=" + content['img']
    });
  },


  changeType : function (e) {
    var value = e.currentTarget.dataset.value;
    var index = e.currentTarget.dataset.index;
    console.log(value);
    getApp().globalData.imgMenuList.currentMenu = index;
    wx.redirectTo({
      url: value
    })
  }
})
