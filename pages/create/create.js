// pages/create/create.js
var imageUtil = require('../../utils/util.js');

Page({
  data: {
    templateId: "vlc",
    templateImg: "https://media.hhui.top/ximg/template/vlc.png",
    signEnable: false,
    signText: "",
    disabled: false,
    contents: "",
    files: [],
    imagewidth: 0,//缩放后的宽  
    imageheight: 0,//缩放后的高  
    leftSize:0,
  },
  onLoad: function (option) {
    var path = getApp().globalData.host;

    console.log("create", option);
    this.setData({
      templateId: option.id,
      templateImg : option.img
    });
  },
  imageLoad: function (e) {
    var imageSize = imageUtil.imageUtil(e)
    this.setData({
      imagewidth: imageSize.imageWidth,
      imageheight: imageSize.imageHeight,
      leftSize : imageSize.left
    })
  },
  chooseImage: function (e) {
    var that = this;
    if (this.data.files.length > 0) {
      this.data.files = [];
    }
    wx.chooseImage({
      count: 1,
      sizeType: ['original', 'compressed'], // 可以指定是原图还是压缩图，默认二者都有
      sourceType: ['album', 'camera'], // 可以指定来源是相册还是相机，默认二者都有
      success: function (res) {
        // 返回选定照片的本地文件路径列表，tempFilePath可以作为img标签的src属性显示图片
        that.setData({
          files: that.data.files.concat(res.tempFilePaths)
        });
      }
    })
  },
  previewImage: function (e) {
    wx.previewImage({
      current: e.currentTarget.id, // 当前显示图片的http链接
      urls: this.data.files // 需要预览的图片http链接列表
    })
  },

  previewImageV2: function (e) {
    wx.previewImage({
      current: this.data.templateImg, // 当前显示图片的http链接
      urls: [this.data.templateImg] // 需要预览的图片http链接列表
    })
  },

  signChange: function(e) {
    this.setData({
      signEnable: e.detail.value
    });
    console.log(this.data);
  },

  imgCreate: function(e) {
    console.log("here", e.detail.value.textarea);
  },

  bindFormSubmit: function (e) {
    if(this.data.files.length ==0 ) {
      wx.showToast({
        title: '请选择一张图片',
        icon: 'success',
        duration: 1500
      });
      return;
    }

    if (this.data.disabled) {
      return;
    }

    // 设置btn为不可点击
    this.setData({disabled: true});
    wx.showToast({
      title: '努力生成中...',
      icon: 'loading'
    });

    var path = getApp().globalData.host;
    var that = this;


    // 设置提交的参数
    var msg = e.detail.value.textarea;
    // var msg = "酬杜八同宿西山兰若\n周弼\n\n西山龙井路，俱有旧行踪。\n待得城中暇，重期石上逢。\n冷霜粘破屐，落月带残钟。\n拟践相邻约，烦君买一峰。";
    var id = this.data.templateId;
    var tempFile = this.data.files[0];
    var signEnable = this.data.signEnable ? 1 : 0;
    var sign = e.detail.value.signarea;
    console.log("request", signEnable, sign);
    wx.uploadFile({
      url: path + 'wx/wx/create',
      // url: 'http://127.0.0.1:8080/wx/create', //仅为示例，非真实的接口地址
      header: {
        'Content-Type': 'application/json'
      },
      filePath: tempFile,
      name: 'image',
      formData: {
        'msg': msg,
        "templateId" : id,
        "signStatus" : signEnable,
        "sign" : sign
      },
      success: function (res) {
        wx.hideToast();
        that.setData({disabled:false});

        var data = res.data;
        
        var ans = JSON.parse(data);
        console.log(ans);
        if(ans['status']['code'] == 200) {
          // var imgUrl = ans['result']['prefix'] + ans['result']['base64result'];
          var imgUrl = path + ans.result.img;
          console.log("img", imgUrl);
          wx.previewImage({
            current: imgUrl, // 当前显示图片的http链接
            urls: [imgUrl] // 需要预览的图片http链接列表
          });

          

        } else {
            wx.showToast({
              title: '生成失败',
              icon: 'loading',
              duration: 1500
            });
        }
      }, 
      fail: function() {
        wx.showToast({
          title: '生成失败!',
          duration: 1500
        });
        that.setData({ disabled: false });
      }
    })

  }


});