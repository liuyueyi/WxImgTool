// pages/create/qrcode/gen/gen.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    menuList:{},
    logoEnable: false,  // 是否开启签名
    files: [],
    errorLevel: [
      "低",
      "中",
      "高",
      "超高"
    ],
    errorLevelIndex: 0,
    submitDisabled: false,
    containPadding : true
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    // 设置menu
    var menu = getApp().globalData.imgMenuList;
    this.setData({
      menuList: menu
    });
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
  
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
  
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
  
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
  
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
  
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
  
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
  
  },

  changeType: function (e) {
    var value = e.currentTarget.dataset.value;
    var index = e.currentTarget.dataset.index;
    console.log(value);

    if (getApp().globalData.imgMenuList.currentMenu == index) {
      return;
    }

    getApp().globalData.imgMenuList.currentMenu = index;
    wx.redirectTo({
        url: value
      })
    // if (value == "/pages/create/template/template") {
    //   wx.switchTab({
    //     url: value
    //   })
    // } else {
    //   wx.navigateTo({
    //     url: value
    //   })
    // }
  },



  // 图片选择
  chooseMainImg: function (e) {
    var that = this;
    wx.chooseImage({
      count: 1,
      sizeType: ['original', 'compressed'], // 可以指定是原图还是压缩图，默认二者都有
      sourceType: ['album', 'camera'], // 可以指定来源是相册还是相机，默认二者都有
      success: function (res) {
        // 返回选定照片的本地文件路径列表，tempFilePath可以作为img标签的src属性显示图片
        that.setData({
          img: res.tempFilePaths
        });
      }
    })
  },

  previewImage: function (e) {
    wx.previewImage({
      current: e.currentTarget.id, // 当前显示图片的http链接
      urls: [e.currentTarget.id] // 需要预览的图片http链接列表
    })
  },


  // logoEnable change
  logoEnableChange: function (e) {
    this.setData({
      logoEnable: e.detail.value
    });
  },
  cancelPadding: function (e) {
    this.setData({
      containPadding: e.detail.value
    });
  },

  bindLevelChange: function (e) {
    var index = e.detail.value;
    this.data.set({
      errorLevelIndex: index
    });
  },

  bindFormSubmit: function (e) {
    var content = e.detail.value.textarea;
    if(content == null || content.length == 0) {
      console.log("内容为空!");
      return;
    }

    var size = e.detail.value.logoSize;
    var errorLevel = this.data.errorLevelIndex;
    var padding = this.data.containPadding ? 1: 0;
    var params = {
      size: size,
      content: content,
      error: errorLevel,
      padding: padding
    }
    var path = getApp().globalData.host;
    var that = this;


    this.data.submitDisabled = true;
    
    var tempFile = '';
    if (this.data.logoEnable && this.data.img && this.data.img.length==1) {
      tempFile = this.data.img[0];
      console.log("upload logo!");
      wx.uploadFile({
        url: path + 'wx/wx/qrcode/encode',
        // url: 'http://127.0.0.1:8080/wx/qrcode/encode', //仅为示例，非真实的接口地址
        header: {
          'Content-Type': 'application/json'
        },
        filePath: tempFile,
        name: 'image',
        formData: params,
        success: function (res) {
          wx.hideToast();
          that.setData({ submitDisabled: false });

          var data = res.data;

          var ans = JSON.parse(data);
          console.log(ans);
          if (ans['status']['code'] == 200) {
            var imgUrl = path + ans.result.img;
            console.log("img", imgUrl);
            wx.previewImage({
              current: imgUrl, // 当前显示图片的http链接
              urls: [imgUrl] // 需要预览的图片http链接列表
            });
          } else {
            wx.showToast({
              title: '生成失败',
              icon: 'loading',
              duration: 1500
            });
          }
        },
        fail: function () {
          wx.showToast({
            title: '生成失败!',
            duration: 1500
          });
          that.setData({ submitDisabled: false });
        }
      })
    } else {
      wx.request({
        url: path + 'wx/wx/qrcode/encode',
        // url: 'http://127.0.0.1:8080/wx/qrcode/encode', //仅为示例，非真实的接口地址
        header: {
          'Content-Type': 'application/json'
        },
        data: params,
        success: function (res) {
          wx.hideToast();
          that.setData({ submitDisabled: false });

          var ans = res.data;

          // var ans = JSON.parse(data);
          console.log(ans);
          if (ans['status']['code'] == 200) {
            var imgUrl = path + ans.result.img;
            console.log("img", imgUrl);
            wx.previewImage({
              current: imgUrl, // 当前显示图片的http链接
              urls: [imgUrl] // 需要预览的图片http链接列表
            });
          } else {
            wx.showToast({
              title: '生成失败',
              icon: 'loading',
              duration: 1500
            });
          }
        },
        fail: function () {
          wx.showToast({
            title: '生成失败!',
            duration: 1500
          });
          that.setData({ submitDisabled: false });
        }
      })
    }
    

  },





  
})