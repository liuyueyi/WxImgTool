
Page({

  /**
   * 页面的初始数据
   */
  data: {
    menuList: {},
    files: [],
    submitDisabled: false,
    ans: ""
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    // 设置menu
    var menu = getApp().globalData.imgMenuList;
    this.setData({
      menuList: menu
    });
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  },

  changeType: function (e) {
    var value = e.currentTarget.dataset.value;
    var index = e.currentTarget.dataset.index;
    console.log(value);

    if (getApp().globalData.imgMenuList.currentMenu == index) {
      return;
    }

    getApp().globalData.imgMenuList.currentMenu = index;
    wx.redirectTo({
      url: value
    })
    // if (value == "/pages/create/template/template") {
    //   wx.switchTab({
    //     url: value
    //   })
    // } else {
    //   wx.navigateTo({
    //     url: value
    //   })
    // }
  },



  // 图片选择
  chooseMainImg: function (e) {
    var that = this;
    wx.chooseImage({
      count: 1,
      sizeType: ['original', 'compressed'], // 可以指定是原图还是压缩图，默认二者都有
      sourceType: ['album', 'camera'], // 可以指定来源是相册还是相机，默认二者都有
      success: function (res) {
        // 返回选定照片的本地文件路径列表，tempFilePath可以作为img标签的src属性显示图片
        that.setData({
          img: res.tempFilePaths
        });
      }
    })
  },

  previewImage: function (e) {
    wx.previewImage({
      current: e.currentTarget.id, // 当前显示图片的http链接
      urls: [e.currentTarget.id] // 需要预览的图片http链接列表
    })
  },



  bindFormSubmit: function (e) {
    var tempFile = this.data.img;
    if (tempFile == null) {
      wx.showToast({
        title: '请选择一张二维码图',
        icon: 'success',
        duration: 1500
      });
      return;
    }

    var path = getApp().globalData.host;
    var that = this;
    tempFile = tempFile[0];
    wx.uploadFile({
      url: path + 'wx/wx/qrcode/decode',
      // url: 'http://127.0.0.1:8080/wx/qrcode/decode', //仅为示例，非真实的接口地址
      header: {
        'Content-Type': 'application/json'
      },
      filePath: tempFile,
      name: 'image',
      success: function (res) {
        wx.hideToast();


        var data = res.data;

        var ans = JSON.parse(data);
        console.log(ans);

        that.setData({ submitDisabled: false, ans: ans.result });
      },
      fail: function () {
        wx.showToast({
          title: '解析失败!',
          duration: 1500
        });
        that.setData({ submitDisabled: false });
      }
    })
  },






})